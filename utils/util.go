package utils

import (
	"context"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"github.com/bitly/go-simplejson"
	"github.com/gin-gonic/gin"
	"gitlab.com/hyshore/combo/logs"
)

func GetCookieInt64(c *gin.Context, key string, defaultVal int64) int64 {
	var (
		strVal string
		err    error
	)
	if strVal, err = c.Cookie(key); err != nil {
		return defaultVal
	}
	if val, err := strconv.ParseInt(strVal, 10, 64); err == nil {
		return val
	}
	return defaultVal
}
func GetQueryParamString(c *gin.Context, key string, defaultVal string) string {
	var strVal = c.Query(key)

	if strVal == "" {
		return defaultVal
	}

	return strVal
}

func GetQueryParamInt64(c *gin.Context, key string, defaultVal int64) int64 {
	var (
		strVal string
		val    int64
		err    error
	)

	strVal = c.Query(key)
	if strVal == "" {
		return defaultVal
	}
	if val, err = strconv.ParseInt(strVal, 10, 64); err != nil {
		return defaultVal
	}
	return val
}

func IsContainWords(ss string, words []string) bool {
	if len(ss) == 0 {
		return false
	}

	ss = strings.ToLower(ss)
	for _, word := range words {
		if len(word) == 0 {
			continue
		}
		if strings.Contains(ss, strings.ToLower(word)) {
			return true
		}
	}
	return false
}

func TrimArray(words []string) []string {
	ret := make([]string, 0, len(words))
	for _, word := range words {
		if word == "" {
			continue
		}
		ret = append(ret, word)
	}
	return ret
}

func LoadJsonFromHttp(ctx context.Context, url string) (*simplejson.Json, error) {
	resp, err := http.Get(url)
	if err != nil {
		logs.CtxWarn(ctx, "Get Url:%v, err:%v", url, err)
		return nil, err
	}

	defer resp.Body.Close()
	buf, err := ioutil.ReadAll(resp.Body)
	logs.CtxInfo(ctx, "url:%v, resp:%+v", url, string(buf))
	if err != nil {
		logs.CtxWarn(ctx, "ReadAll Url:%v, err:%v", url, err)
		return nil, err
	}
	return simplejson.NewJson(buf)
}

func IsPC(ua string) bool {
	return false
	var items = []string{
		"mac",
		"window",
		"macintosh",
	}

	return IsContainWords(ua, items)
}

func IsBaiduSpider(UA string) bool {
	return IsContainWords(UA, []string{"spider", "render"})
}
