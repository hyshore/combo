package utils

import (
	"context"
	"net/url"
	"strings"
)

func ParseQueryFromUrl(_ context.Context, urlStr string) (url.Values, error) {
	var idx = strings.Index(urlStr, "?")
	if idx < 0 || idx >= len(urlStr) {
		idx = 0
	}
	var trimUrl = urlStr[idx:]
	return url.ParseQuery(trimUrl)
}
