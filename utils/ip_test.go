package utils

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"testing"
)

func Test_GetClientIP(t *testing.T) {
	var c = &gin.Context{
		Request: &http.Request{
			Header: make(http.Header),
		},
	}
	c.Request.Header.Set("Ali-Cdn-Real-Ip", "10.0.0.1")
	c.Request.Header.Set("X-Real-Ip", "10.0.0.1")
	c.Request.Header.Set("X-Forwarded-for", "10.0.0.1, 199.168.1.1")
	c.Request.RemoteAddr = "1.1.1.1:80"
	t.Log(GetClientIP(c))
}
