package utils

import (
	//"fmt"
	"github.com/gin-gonic/gin"
	"net"
	"strings"
)

var (
	ipnetStrings = []string{
		"10.0.0.0/8",
		"172.16.0.0/12",
		"192.168.0.0/16",
	}
	ipnets = []*net.IPNet{}
)

func isLocalIP(ipStr string) bool {
	ip := net.ParseIP(ipStr)
	for _, ipnet := range ipnets {
		if ipnet.Contains(ip) {
			return true
		}
	}
	return false
}
func init() {
	for _, ipnetString := range ipnetStrings {
		_, ipnet, err := net.ParseCIDR(ipnetString)
		if err != nil {
			panic(err)
		}
		ipnets = append(ipnets, ipnet)
	}
}

func GetXRealIP(c *gin.Context) string {
	var xRealIP = strings.TrimSpace(c.Request.Header.Get("X-Real-Ip"))
	if xRealIP != "" && !isLocalIP(xRealIP) {
		return xRealIP
	}
	return ""
}

func GetXForwardedForIP(c *gin.Context) string {
	var clientIPs = strings.Split(c.Request.Header.Get("X-Forwarded-For"), ",")
	for _, cip := range clientIPs {
		var cIP = strings.TrimSpace(cip)
		if cIP == "" {
			continue
		}
		if isLocalIP(cIP) {
			continue
		}
		return cIP
	}
	return ""
}

func GetAllIPList(c *gin.Context) []string {
	var ret = make([]string, 0, 10)
	var clientIPs = strings.Split(c.Request.Header.Get("X-Forwarded-For"), ",")
	for _, cip := range clientIPs {
		var cIP = strings.TrimSpace(cip)
		if cIP == "" {
			continue
		}
		if isLocalIP(cIP) {
			continue
		}
		ret = append(ret, cIP)
	}

	if cip := GetXRealIP(c); cip != "" {
		ret = append(ret, cip)
	}

	return ret
}

func GetClientIP(c *gin.Context) (string, string) {
	//
	var aliCdnRealIP = strings.TrimSpace(c.Request.Header.Get("Ali-Cdn-Real-Ip"))
	if len(aliCdnRealIP) > 0 && !isLocalIP(aliCdnRealIP) {
		return aliCdnRealIP, "alicdnrealip"
	}
	// 很奇怪:  anroid 貌似都 在x-real-ip 上, 不明所以，但就这样吧
	if cip := GetXRealIP(c); cip != "" && !isLocalIP(cip) {
		return cip, "x_real_ip"
	}

	if cip := GetXForwardedForIP(c); cip != "" {
		return cip, "x_forward"
	}

	if addr := c.Request.Header.Get("X-Appengine-Remote-Addr"); addr != "" && !isLocalIP(addr) {
		return addr, "app_engine"
	}

	if ip, _, err := net.SplitHostPort(strings.TrimSpace(c.Request.RemoteAddr)); err == nil {
		if !isLocalIP(ip) {
			return ip, "remote_ip"
		}
	}
	return "", "empty"
}
