package utils

import (
	"os"
)

func GetWho() string {
	return os.Getenv("who")
}
