package utils

func UniqSliceString(ss []string) []string {
	var ret = make([]string, 0, len(ss))
	var m = make(map[string]bool)
	for _, s := range ss {
		if m[s] {
			continue
		}
		ret = append(ret, s)
		m[s] = true
	}
	return ret
}
