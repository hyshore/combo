package utils

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"gitlab.com/hyshore/combo/logs"
)

func Post(ctx context.Context, url string, headers map[string]string, buf []byte) ([]byte, error) {
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(buf))
	if err != nil {
		logs.CtxWarn(ctx, "PostErr:%v", err)
		return nil, fmt.Errorf("new request is fail, err:%w", err)
	}
	for key, val := range headers {
		req.Header.Set(key, val)

	}
	client := &http.Client{
		Timeout: time.Second * 5,
	}
	response, err := client.Do(req)
	if err != nil {
		logs.CtxWarn(ctx, "Post Request err:%v", err)
		return nil, err
	}
	defer response.Body.Close()
	if response.StatusCode != http.StatusOK {
		err = fmt.Errorf("StatusCode:%v", response.Status)
		logs.CtxWarn(ctx, "PostResp:status=%v, resp=%v", response.Status, response)
		return nil, err
	}
	ret, err := ioutil.ReadAll(response.Body)
	logs.CtxInfo(ctx, "PostInfo, reqData:%v, response:%v, err:%v", string(buf), string(ret), err)
	return ret, err

}

func Get(ctx context.Context, url string, headers map[string]string) ([]byte, error) {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		logs.CtxWarn(ctx, "GetErr:%v", err)
		return nil, fmt.Errorf("new request is fail, err:%w", err)
	}

	for key, val := range headers {
		req.Header.Set(key, val)
	}

	client := &http.Client{
		Timeout: time.Second * 5,
	}
	response, err := client.Do(req)
	if err != nil {
		logs.CtxWarn(ctx, "Get Request err:%v", err)
		return nil, err
	}
	defer response.Body.Close()
	if response.StatusCode != http.StatusOK {
		err = fmt.Errorf("StatusCode:%v", response.Status)
		logs.CtxWarn(ctx, "GetResp:status=%v, resp=%v", response.Status, response)
		return nil, err
	}
	ret, err := ioutil.ReadAll(response.Body)
	logs.CtxInfo(ctx, "GetInfo, reqData:%v, response:%v, err:%v", url, string(ret), err)
	return ret, err

}
