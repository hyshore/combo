package utils

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"strings"
)

func GenPerDomainFP(guestDomain string, ip string, userAgent string) string {
	var s = strings.Replace(fmt.Sprintf("%s%s%s", guestDomain, ip, userAgent), " ", "", -1)
	var hasher = md5.New()
	hasher.Write([]byte(s))
	return hex.EncodeToString(hasher.Sum(nil))
}

func GenGlobalFP(ip string, userAgent string) string {
	var s = strings.Replace(fmt.Sprintf("%s%s", ip, userAgent), " ", "", -1)
	var hasher = md5.New()
	hasher.Write([]byte(s))
	return hex.EncodeToString(hasher.Sum(nil))
}
