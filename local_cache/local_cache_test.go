package local_cache

import (
	"context"
	"fmt"
	"testing"
	"time"
)

func loader(ctx context.Context, key string) (interface{}, error) {
	fmt.Printf("key=%s\n", key)
	return map[string]interface{}{
		"abc": "abc",
		"efg": "efg",
	}[key], nil
}

func TestGet(t *testing.T) {
	var l = NewLocalCacheLoader(loader, time.Second)
	var ctx = context.TODO()
	var val, err = l.Get(ctx, "abc")
	t.Log(err)
	t.Log(val)
	val, err = l.Get(ctx, "abc")
	t.Log(err)
	t.Log(val)
	time.Sleep(time.Second)
	val, err = l.Get(ctx, "abc")
	t.Log(err)
	t.Log(val)
	val, err = l.Get(ctx, "hlk")
	t.Log(err)
	t.Log(val)
	t.Log(val == nil)
}
