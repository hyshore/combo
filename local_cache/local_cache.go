package local_cache

import (
	"context"
	"time"

	gocache "github.com/patrickmn/go-cache"
	"gitlab.com/hyshore/combo/logs"
)

type Loader func(context.Context, string) (interface{}, error)

type LocalCacheLoader struct {
	loader  Loader
	timeout time.Duration
	storage *gocache.Cache
}

func NewLocalCacheLoader(loader Loader, timeout time.Duration) *LocalCacheLoader {
	return &LocalCacheLoader{
		loader:  loader,
		timeout: timeout,
		storage: gocache.New(timeout, timeout*5),
	}
}

func (l *LocalCacheLoader) Get(ctx context.Context, key string) (interface{}, error) {
	var o, ok = l.storage.Get(key)
	if ok {
		return o, nil
	}
	// else
	var err error
	o, err = l.loader(ctx, key)
	if err != nil {
		logs.CtxWarn(ctx, "load key=%v, err:%v", key, err)
		return nil, err
	}
	l.storage.SetDefault(key, o)
	return o, nil
}
