package logs

import (
	"context"
	"testing"
)

func TestCtxInfo(t *testing.T) {
	var ctx = context.TODO()
	ctx = WithLogId(ctx, "123")
	CtxInfo(ctx, "abc:%v", 123)
}
