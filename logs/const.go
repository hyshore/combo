package logs

const (
	FILE_NAME_KEY = "_FILE_NAME"
	LINE_KEY      = "_LINE_NUM"
	LOG_ID_KEY    = "_LOG_ID"

	traceCtxKey = "_K_Trace"
	logIdCtxKey = "_K_LogId"
)
