package logs

import (
	"bytes"
	"fmt"
	"strings"

	"github.com/sirupsen/logrus"
)

type LogFormat struct {
	TimestampFormat string
}

func (f *LogFormat) Format(entry *logrus.Entry) ([]byte, error) {
	var b *bytes.Buffer

	if entry.Buffer != nil {
		b = entry.Buffer
	} else {
		b = &bytes.Buffer{}
	}

	b.WriteByte('[')
	b.WriteString(strings.ToUpper(entry.Level.String()))
	b.WriteString("] ")
	b.WriteString(entry.Time.Format(f.TimestampFormat))

	if fileName, found := entry.Data[FILE_NAME_KEY]; found {
		b.WriteString(" ")
		strFileName := fileName.(string)
		ss := strings.Split(strFileName, "/")
		if len(ss) > 0 {
			strFileName = ss[len(ss)-1]
		}
		b.WriteString(strFileName)
		b.WriteString(":")
		delete(entry.Data, FILE_NAME_KEY)
	}

	if line, found := entry.Data[LINE_KEY]; found {
		fmt.Fprint(b, line)
		b.WriteString(" ")
		delete(entry.Data, LINE_KEY)
	}

	if logId, found := entry.Data[LOG_ID_KEY]; found {
		var logId, _ = logId.(string)
		if len(logId) > 0 {
			fmt.Fprint(b, logId)
			b.WriteString(" ")
		}
		delete(entry.Data, LOG_ID_KEY)
	} else {
		fmt.Fprint(b, "- ")
	}

	if entry.Message != "" {
		b.WriteString(" - ")
		b.WriteString(entry.Message)
		b.WriteString(" ")
	}

	for key, value := range entry.Data {
		b.WriteString(key)
		b.WriteByte('=')
		fmt.Fprint(b, value)
		b.WriteByte(' ')
	}

	b.WriteByte('\n')
	return b.Bytes(), nil
}
