package logs

import (
	"context"
	"sync"
)

func GetTrace(ctx context.Context) *TraceKV {
	if ntc, ok := ctx.Value(traceCtxKey).(*TraceKV); ok {
		return ntc
	}
	return nil
}

type TraceKV struct {
	kvs   []interface{}
	mutex sync.Mutex
}

func NewTraceKV() *TraceKV {
	return &TraceKV{
		kvs: make([]interface{}, 0),
	}
}

func (h *TraceKV) PushTrace(key interface{}, val interface{}) {
	h.mutex.Lock()
	h.kvs = append(h.kvs, key, val)
	h.mutex.Unlock()
}

func (h *TraceKV) GetKVs() []interface{} {
	h.mutex.Lock()
	defer h.mutex.Unlock()
	kvs := h.kvs
	return kvs
}
