package logs

import (
	"context"
	"fmt"
	"runtime"
	"strings"

	"github.com/sirupsen/logrus"
)

// 显式初始化
func Init(lvl, filePath string) {
	// logrus.SetFormatter(&LogFormat{})
	logrus.AddHook(newLfsHook(lvl, filePath))
}

func trimFileName(filename string) string {
	ss := strings.Split(filename, "/")
	if len(ss) > 0 {
		return ss[len(ss)-1]
	}
	return filename
}

func WithLogId(ctx context.Context, logId string) context.Context {
	return context.WithValue(ctx, logIdCtxKey, logId)
}

func GetLogId(ctx context.Context) string {
	var logId, _ = ctx.Value(logIdCtxKey).(string)
	return logId
}

func CtxInfo(ctx context.Context, format string, args ...interface{}) {
	s := format
	if len(args) > 0 {
		s = fmt.Sprintf(format, args...)
	}
	_, file, line, _ := runtime.Caller(1)
	logrus.WithFields(map[string]interface{}{
		FILE_NAME_KEY: trimFileName(file),
		LINE_KEY:      line,
		LOG_ID_KEY:    GetLogId(ctx),
	}).Info(s)
}

func Infof(format string, args ...interface{}) {
	CtxInfo(context.TODO(), format, args...)
}

func CtxWarn(ctx context.Context, format string, args ...interface{}) {
	s := format
	if len(args) > 0 {
		s = fmt.Sprintf(format, args...)
	}
	_, file, line, _ := runtime.Caller(1)
	logrus.WithFields(map[string]interface{}{
		FILE_NAME_KEY: trimFileName(file),
		LINE_KEY:      line,
		LOG_ID_KEY:    GetLogId(ctx),
	}).Warn(s)
}

func CtxError(ctx context.Context, format string, args ...interface{}) {
	s := format
	if len(args) > 0 {
		s = fmt.Sprintf(format, args...)
	}
	_, file, line, _ := runtime.Caller(1)
	logrus.WithFields(map[string]interface{}{
		FILE_NAME_KEY: trimFileName(file),
		LINE_KEY:      line,
		LOG_ID_KEY:    GetLogId(ctx),
	}).Error(s)
}

func Errorf(format string, args ...interface{}) {
	CtxError(context.TODO(), format, args...)
}

func CtxDebug(ctx context.Context, format string, args ...interface{}) {
	s := format
	if len(args) > 0 {
		s = fmt.Sprintf(format, args...)
	}
	_, file, line, _ := runtime.Caller(1)
	logrus.WithFields(map[string]interface{}{
		FILE_NAME_KEY: trimFileName(file),
		LINE_KEY:      line,
		LOG_ID_KEY:    GetLogId(ctx),
	}).Debug(s)
}

func CtxTrace(ctx context.Context, format string, args ...interface{}) {
	//logrus.Trace(args)
	s := format
	if len(args) > 0 {
		s = fmt.Sprintf(format, args...)
	}
	_, file, line, _ := runtime.Caller(1)
	logrus.WithFields(map[string]interface{}{
		FILE_NAME_KEY: trimFileName(file),
		LINE_KEY:      line,
		LOG_ID_KEY:    GetLogId(ctx),
	}).Trace(s)
}

func CtxPushTrace(ctx context.Context, key, val interface{}) {
	//	logrus.Trace(args)
	ntc := GetTrace(ctx)
	if ntc == nil {
		return
	}

	ntc.PushTrace(key, val)
}

func CtxFlushTrace(ctx context.Context) {
	//	logrus.Trace(args)
	ntc := GetTrace(ctx)
	if ntc == nil {
		return
	}

	var (
		kvs              = ntc.GetKVs()
		idx              = 0
		_, file, line, _ = runtime.Caller(1)
		fields           = map[string]interface{}{
			FILE_NAME_KEY: trimFileName(file),
			LINE_KEY:      line,
			LOG_ID_KEY:    GetLogId(ctx),
		}
	)
	for ; idx+1 < len(kvs); idx += 2 {
		fields[fmt.Sprintf("%v", kvs[idx])] = kvs[idx+1]
	}

	logrus.WithFields(fields).Trace()

}

func NewTraceCtx(ctx context.Context) context.Context {
	ntc := NewTraceKV()
	return context.WithValue(ctx, traceCtxKey, ntc)
}
